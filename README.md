## Visual Step Sequencer

trigger MIDI/OSC messages from abstract visuals

16*16 steps sequencer, each visual step with its own duration and BPM

by gllm, 2019
