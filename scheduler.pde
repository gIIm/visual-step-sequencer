class Scheduler {
  
  // Constructor
  Scheduler(int _bpm, int _length) {
    bpm = _bpm;
    counter = 0;
    currentTime = 0;
    minuteLength = 60000;
    clipIndex = 0;
    clipLength = _length;
    stepIndex = 0;
    stepLength = minuteLength / bpm;
  };
  
  int bpm;
  int counter;
  int currentTime;
  int minuteLength;
  int clipIndex;
  int clipLength;
  int stepIndex;
  int stepLength;
    
  void updateBpm(int _bpm) {
    bpm = _bpm;
  }
  
  void updateClipLength(int _length) {
    clipLength = _length;
  }
  
  void update(){
    counter = millis() - currentTime;
    
    /*
    if (counter > clipLength) {
      reset();
    }
    */
  }
  
  
  void reset(){
    currentTime = millis();
    counter = 0;
    // stepIndex = 0;
    stepLength = minuteLength / bpm;
  }
  
  /*
  void resetClip(){
    currentTime = millis();
    counter = 0;
    stepIndex = 0;
    stepLength = minuteLength / bpm;
  }

  void resetStep(){
    currentTime = millis();
    counter = 0;
    stepLength = minuteLength / bpm;
  }
  */
}
