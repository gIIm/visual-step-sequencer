/**
* Visual Step Sequencer
*
* trigger MIDI/OSC messages from abstract visuals
* 16*16 steps sequencer, each visual step with its own duration and BPM
* by gllm, 2019
*
**/

import controlP5.*;

ControlP5 cp5;
Slider bpmSlider;
Slider lengthSlider;

PFont mono;

Scheduler scheduler;

Sequencer sequencer;

int bpm = 120;
int clipLength = 10000;

void setup() {
 size(640, 480);
 frameRate(30);
 
 background(34, 34, 34);
 stroke(255);
 
 scheduler = new Scheduler(bpm, clipLength);
  
 sequencer = new Sequencer(scheduler);  
 sequencer.setClips(16);
 sequencer.setSteps(16, 1, 60, 127);

 setGui();
}

void draw() {
 background(34, 34, 34);
 
 drawDebug();
 
 scheduler.update();

 sequencer.update(scheduler);
 sequencer.drawClipLength(scheduler.counter);
 sequencer.drawClips();
 sequencer.drawSteps();
   
}

void drawDebug() {
 // String[] fontList = PFont.list();
 // printArray(fontList);
 // mono= loadFont("font/New-Alphabet-16.vlw");
 mono = createFont("Courier New", 16);
 textFont(mono);

 fill(255);

 text("BPM: " + scheduler.bpm, 20, 40);
 text("CLIP LENGTH: " + scheduler.clipLength, 20, 60);
 text("CLIP ID: " + sequencer.currentClipIndex, 20, 80);
 text("STEP ID: " + sequencer.clips[sequencer.currentClipIndex].stepIndex, 20, 100);
 text("COUNTER: " + scheduler.counter, 20, 120);
 text("TOTAL TIME: " + millis(), 20, 140);

}

void setGui() {
  cp5 = new ControlP5(this);

  bpmSlider = cp5.addSlider("updateBpmSlider")
               .setRange(10, 240)
               .setValue(bpm)
               .setLabel("bpm")
               .setPosition(220, 30)
               .setWidth(80)
               .setSliderMode(Slider.FIX);

  lengthSlider = cp5.addSlider("updateClipLengthSlider")
               .setRange(1000, 60000)
               .setValue(clipLength)
               .setLabel("clip length")
               .setPosition(220, 50)
               .setWidth(80)
               .setSliderMode(Slider.FIX);
}

void updateBpmSlider(int _bpm) {
  scheduler.updateBpm(_bpm);
}

void updateClipLengthSlider(int _length) {
  scheduler.updateClipLength(_length);
}

void mousePressed() { 
  //sequencer.getClipId(mouseX, mouseY);
  sequencer.setClipParameters(mouseX, mouseY, bpmSlider);
}
