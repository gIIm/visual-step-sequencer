class Clip {
  
  // Constructor
  Clip(int _id, int _bpm, int _duration) {
    id = _id;
    bpm = _bpm;
    duration = _duration;
    stepIndex = 0;
    isActive = false;
    isReset = false;
  };
  
  int id;
  int bpm;
  int duration;
  int stepIndex;
  
  boolean isActive;
  boolean isReset;
  
  Step[] steps;
  
  void setSteps(int _stepsNum, int _channel, int _note, int _velocity) {
    steps = new Step[_stepsNum];  
    
    for (int i = 0; i < steps.length; i++) {   
      steps[i] = new Step(i, _channel, _note, _velocity);
    }
  }
    
  void updateSteps(Scheduler _scheduler) {
    if (_scheduler.counter >= _scheduler.stepLength) {
      
      _scheduler.stepLength += _scheduler.minuteLength/_scheduler.bpm;      
        
      if (_scheduler.stepIndex < steps.length - 1 && !isReset) {
        _scheduler.stepIndex++;
      } else {
        if (!isReset) {
          _scheduler.stepIndex = 0;
        } else {
          _scheduler.stepIndex = 1;
        }
        isReset = false;
      }
      
      stepIndex = _scheduler.stepIndex;
            
      for (int i = 0; i < steps.length; i++) {
        steps[i].isActive = false;
        if ( i == stepIndex ) steps[i].isActive = true;
      }
  
      //steps[stepIndex].bang();
    }
  
  }
  
  void resetSteps() {
    steps[0].isActive = true;
    
    for (int i = 1; i < steps.length; i++) {
      steps[i].isActive = false;
    }    
    
    isReset = true;
  }
  
  void bang() {
    println("CLIP ID: " + id);
  }
  
}
