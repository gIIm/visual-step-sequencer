class Step {
  
  // Constructor
  Step(int _id, int _channel, int _note, int _velocity) {
    id = _id;
    channel = _channel;
    note = _note;
    velocity = _velocity;
    isActive = false;
  };
  
  int id;
  int channel;
  int note;
  int velocity;
  
  boolean isActive;
  
  void bang() {
    println("STEP ID: " + id);
  }
  
}
